package tk.maurocerbai.fifteen;

import java.util.ArrayList;
import java.util.Random;
import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;

public class ButtonAdapter extends BaseAdapter implements OnClickListener
{
	private Context mContext;
	private static int positionNull;
	static int [] filesnames;
	private static ArrayList <Integer> list;
	private static int size;
	MainActivity main_activity;

	public ButtonAdapter(Context c,MainActivity main)
	{
		main_activity=main;
		size = 4;
		mContext = c;
		positionNull=15;
		filesnames = new int [size*size];
		list = new ArrayList <Integer> (size*size-1);
		for(int i = 1; i <= (size*size-1); i++) 
		{
			list.add(i);
		}
		for (int i=0;i<filesnames.length;i++)
			filesnames[i]=shuffle();
        checkIfsolvable();
	}

    private void checkIfsolvable() {
        int tmp=0;
        for (int i=1;i<filesnames.length;i++)
        {
            for (int j=i;j<0;j--)
            {
                tmp += filesnames[j];
            }
            if(tmp==0)
                if (MainActivity.debug) Log.v("checkIfsolvable","Non ricordo!!");
            main_activity.toast("Puzzle finito");
        }

    }

    private int shuffle ()
	{
        Random rand = new Random();
        while(list.size() > 0)
        {
            int index = rand.nextInt(list.size());
            return(list.remove(index));
        }
        return -1;
	}
	
	@Override
	public int getCount() 
	{
		return filesnames.length;
	}

	@Override
	public Object getItem(int arg0) 
	{
		return null;
	}

	@Override
	public long getItemId(int position) 
	{
		return position;
	}

	@Override
	public View getView (int position, View convertView, ViewGroup parent) 
	{
		Button btn;
		if(convertView == null)
		{
			btn = new Button (mContext);
			btn.setLayoutParams(new GridView.LayoutParams(100,55));
			btn.setPadding(8,8,8,8);
		}
		else 
		{  
			btn = (Button) convertView;
		}
		String writeIntoButton=Integer.toString(filesnames[position]);
		if(position != positionNull)
			btn.setText(writeIntoButton);  // filenames is an array of int 
		else
			btn.setText("");
		btn.setTextColor(Color.WHITE);  
		btn.setId(position);
		if(position == positionNull)
			btn.setEnabled(false);
		btn.setOnClickListener(this);
		btn.setId(position);
		return btn;
	}

	void chagePositionWithNull(int position)
	{
		if(positionNull==position+1 || positionNull==position-1 || positionNull==position+size || positionNull==position-size)
		{
			int tmp = filesnames [positionNull];
			filesnames [positionNull] = filesnames[position];
			filesnames[position] = tmp;
			positionNull = position;
			if (MainActivity.debug)
				for (int i=0;i<filesnames.length;i++)
					Log.v("ButtonAdapter","String di "+i+" : "+filesnames[i]);
			main_activity.repaint();
		}
		else
		{
			if (MainActivity.debug) Log.v("ButtonAdapter","Click di un bottone non adiacente");
			main_activity.vib();
            main_activity.toast("Puzzle finito");
		}
		
	}
	
	static boolean checkIfFinish()
	{
		boolean finish = true;
		for (int i=0;i<filesnames.length-2 && finish;i++)
		{
			if (MainActivity.debug) Log.v("ButtonAdapter","diff: "+Integer.toString(filesnames[i] - filesnames[i+1]));
			if(filesnames[i] - filesnames[i+1] != -1)
			{
				finish = false;
				if (MainActivity.debug) Log.v("ButtonAdapter","dentro if: set finish false: "+finish);
			}
		}
		if (MainActivity.debug) Log.v("ButtonAdapter","finish è settato a "+finish);
		return finish;
	}

	@Override
	public void onClick(View v) 
	{
		Button bt = (Button) v;
		chagePositionWithNull(bt.getId());
	}

}
