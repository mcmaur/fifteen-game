package tk.maurocerbai.fifteen;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Vibrator;
import android.util.Log;
import android.view.Menu;
import android.widget.GridView;
import android.widget.Toast;


public class MainActivity extends Activity 
{
	private GridView gridView;
	private ButtonAdapter btnAda;
	static boolean debug = true;

	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		gridView = (GridView) findViewById(R.id.grid_view);
		btnAda = new ButtonAdapter(this,this);
		if(savedInstanceState != null)
		{
			ButtonAdapter.filesnames = savedInstanceState.getIntArray("tk.maurocerbai.fifteen.adapter");
		}
		gridView.setAdapter(btnAda);
	}

	public void repaint()
	{
		gridView.setAdapter(btnAda);
		if(ButtonAdapter.checkIfFinish())
		{
			if (MainActivity.debug) Log.v("ButtonAdapter","Puzzle finito");
			toast("Puzzle finito");
		}
		else
			Log.v("ButtonAdapter","Puzzle NON finito");
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) 
	{
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}
	
	@Override
	public void onSaveInstanceState(Bundle savedInstanceState)
	{
		savedInstanceState.putIntArray("tk.maurocerbai.fifteen.adapter", ButtonAdapter.filesnames);
		super.onSaveInstanceState(savedInstanceState);
	}
	
	public void vib()
	{
		Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
		v.vibrate(300);
	}
	
	public void toast(String testo)
	{
		Toast.makeText(getApplicationContext(),testo, Toast.LENGTH_SHORT).show();
	}
	
}
